# Lost in translation

Lost in translation is an example project that shows some React fundamentals used to create a simple app translating a string to sign language - with "login" and profile.

You can interact with the example here: [https://aqueous-savannah-60138.herokuapp.com/](https://aqueous-savannah-60138.herokuapp.com/)

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have visual studio code installed (or similar text editor/IDE)
- You have node and npm installed

## Installing Lost in translation

To install Lost in translation, follow these steps:

Open git bash and navigate to the desired location

Paste:
`git clone https://gitlab.com/MortenTobiasNielsen/lost-in-translation`

`cd lost-in-translation`

`npm install`

## Using Lost in translation

To use Lost in translation, follow these steps:

Open the solution in your IDE and run the project.

A websie will open up and you will be able to:

1. Enter a username - will create a new user, if the username cannot be found in the database
2. You are taken to the translation page where you can enter a sentence which will be translated to sign language
3. You can click your avatar in the top right. You will be able to choose your profile or to logout
4. In your profile you can see the last 10 translations you have requested

## Contributing to Lost in translation

To contribute to Lost in translation, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/MortenTobiasNielsen/lost-in-translation/`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

This project uses the following license: [MIT](https://choosealicense.com/licenses/mit/).
