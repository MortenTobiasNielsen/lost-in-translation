import { Dispatch, Middleware } from "redux";
import {
  ACTION_SESSION_CLEAR_ATTEMPT,
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  sessionClearSuccessAction,
  sessionSetAction,
} from "../actions/sessionActions";
import { ActionType, errorUser } from "../../utils/types";
import {
  ACTION_PROFILE_QUERY_USER_ATTEMPT,
  actionQueryUserError,
  actionQueryUserSuccess,
} from "../actions/profileActions";
import { fetchUser } from "../../Components/Login/LoginAPI";
import { sessionStorageName } from "../../utils/connectionParameters";

export const sessionMiddleware: Middleware =
  ({ dispatch }) =>
  (next: Dispatch) =>
  (action: ActionType): void => {
    next(action);

    if (action.type === ACTION_SESSION_INIT) {
      const storedSession = localStorage.getItem(sessionStorageName);

      if (!storedSession) {
        return;
      }

      const userSession = JSON.parse(storedSession);

      dispatch(sessionSetAction(userSession));
    }

    if (action.type === ACTION_SESSION_SET) {
      localStorage.setItem(sessionStorageName, JSON.stringify(action.payload));
    }

    if (action.type === ACTION_SESSION_CLEAR_ATTEMPT) {
      try {
        localStorage.removeItem(sessionStorageName);
      } catch (e) {
        console.log(
          `Could not remove localstorage for ${sessionStorageName} , clearing all localstorage.`
        );
        localStorage.clear();
      }
      dispatch(sessionClearSuccessAction());
    }

    if (action.type === ACTION_PROFILE_QUERY_USER_ATTEMPT) {
      (async () => {
        const { username, id } = action.payload;
        const apiUser = await fetchUser(username);
        if (!apiUser) {
          dispatch(actionQueryUserError(errorUser));
        } else if (apiUser.id === -1 && apiUser.id !== id) {
          dispatch(actionQueryUserError(apiUser));
        }

        dispatch(actionQueryUserSuccess(apiUser));
      })();
    }
  };
