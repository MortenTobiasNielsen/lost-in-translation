import {
  ACTION_TRANSLATE_ATTEMPTING,
  translateErrorAction,
  translateSuccessAction,
} from "../actions/translateActions";
import { Dispatch, Middleware } from "redux";
import { ActionType, errorUser } from "../../utils/types";
import { UpdateTranslations } from "../../Components/Translation/InsertTranslationAPI";
import { fetchUser } from "../../Components/Login/LoginAPI";
import {
  ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT,
  actionClearTranslationsSuccess,
} from "../actions/profileActions";
import { sessionStorageName } from "../../utils/connectionParameters";

export const translateMiddleware: Middleware =
  ({ dispatch }) =>
  (next: Dispatch) =>
  (action: ActionType): void => {
    next(action);

    if (action.type === ACTION_TRANSLATE_ATTEMPTING) {
      (async () => {
        const { user, translation } = action.payload;

        const apiUser = await fetchUser(user.username);
        if (!apiUser) {
          dispatch(translateErrorAction(errorUser));
        } else if (apiUser.id === -1 && apiUser.id !== user.id) {
          dispatch(translateErrorAction(apiUser));
        }

        const translations = apiUser.translations;

        const response = await UpdateTranslations(user.id, [
          translation,
          ...translations,
        ]);

        if (!response || response.id === -1) {
          dispatch(translateErrorAction(user));
        }
        dispatch(translateSuccessAction(response));
      })();
    }

    if (action.type === ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT) {
      const clearTranslation = async () => {
        const newUser = await UpdateTranslations(action.payload.id, []);

        if (newUser.id === -1) {
          alert("Couldn't delete your translations, please try again later");
        }

        localStorage.setItem(sessionStorageName, JSON.stringify(newUser));
        dispatch(actionClearTranslationsSuccess(newUser));
      };

      clearTranslation();
    }
  };
