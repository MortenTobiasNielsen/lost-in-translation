import { Dispatch, Middleware } from "redux";
import { createUser, fetchUser } from "../../Components/Login/LoginAPI";
import { ActionType } from "../../utils/types";
import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
  loginErrorAction,
  loginSuccessAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionActions";

export const loginMiddleware: Middleware =
  ({ dispatch }) =>
  (next: Dispatch) =>
  (action: ActionType): void => {
    next(action);

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
      const loginUser = async () => {
        let user = await fetchUser(action.payload);

        if (!user) {
          user = await createUser(action.payload);
        }

        if (user.id === -1) {
          dispatch(loginErrorAction("User fetch/create error"));
          return;
        }

        dispatch(loginSuccessAction(user));
      };

      loginUser();
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
      dispatch(sessionSetAction(action.payload));
    }
  };
