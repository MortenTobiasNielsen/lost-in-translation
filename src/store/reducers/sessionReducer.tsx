import {
  ACTION_SESSION_CLEAR_ATTEMPT,
  ACTION_SESSION_CLEAR_SUCCESS,
  ACTION_SESSION_SET,
} from "../actions/sessionActions";
import { errorUser, ActionType } from "../../utils/types";
import {
  ACTION_TRANSLATE_ATTEMPTING,
  ACTION_TRANSLATE_CLEAR,
  ACTION_TRANSLATE_ERROR,
  ACTION_TRANSLATE_SUCCESS,
} from "../actions/translateActions";
import {
  ACTION_PROFILE_CLEAR_TRANSLATION_SUCCESS,
  ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT,
  ACTION_PROFILE_QUERY_USER_ERROR,
  ACTION_PROFILE_QUERY_USER_SUCCESS,
} from "../actions/profileActions";

const initialState = {
  user: errorUser,
  loggedIn: false,
  sessionError: "",
  activeTranslation: "",
  translateAttempting: false,
  translateError: "",
  clearTranslationsAttempting: false,
  userProfileQueried: false,
  sessionClearAttempt: false,
};

export const sessionReducer = (
  state = initialState,
  action: ActionType
): typeof initialState => {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        ...state,
        user: action.payload,
        loggedIn: true,
      };
    case ACTION_TRANSLATE_SUCCESS:
      return {
        ...initialState,
        activeTranslation: state.activeTranslation,
        loggedIn: true,
        user: action.payload,
      };
    case ACTION_TRANSLATE_ATTEMPTING:
      return {
        ...state,
        translateAttempting: true,
        translateError: "",
        activeTranslation: action.payload.translation,
      };
    case ACTION_TRANSLATE_ERROR:
      return {
        ...state,
        translateAttempting: false,
        translateError: action.payload,
        loggedIn: false,
      };
    case ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT:
      return {
        ...state,
        clearTranslationsAttempting: true,
      };
    case ACTION_PROFILE_CLEAR_TRANSLATION_SUCCESS:
      return {
        ...state,
        user: action.payload,
        clearTranslationsAttempting: false,
      };
    case ACTION_PROFILE_QUERY_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        userProfileQueried: true,
      };
    case ACTION_PROFILE_QUERY_USER_ERROR:
      return {
        ...state,
        user: action.payload,
        userProfileQueried: false,
      };
    case ACTION_TRANSLATE_CLEAR:
      return {
        ...state,
        activeTranslation: "",
      };
    case ACTION_SESSION_CLEAR_ATTEMPT:
      return {
        ...state,
        sessionClearAttempt: true,
      };

    case ACTION_SESSION_CLEAR_SUCCESS:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
