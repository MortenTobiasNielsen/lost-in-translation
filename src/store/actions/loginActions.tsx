import { UserType } from "../../utils/types";

export const ACTION_LOGIN_ATTEMPTING = "[login] ATTEMPT";
export const ACTION_LOGIN_SUCCESS = "[login] SUCCESS";
export const ACTION_LOGIN_ERROR = "[login] ERROR";

export const loginAttemptAction = (username: string) => ({
  type: ACTION_LOGIN_ATTEMPTING,
  payload: username,
});

export const loginSuccessAction = (user: UserType) => ({
  type: ACTION_LOGIN_SUCCESS,
  payload: user,
});

export const loginErrorAction = (error: string) => ({
  type: ACTION_LOGIN_ERROR,
  payload: error,
});
