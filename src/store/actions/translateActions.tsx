import { UserType } from "../../utils/types";

export const ACTION_TRANSLATE_ATTEMPTING = "[translate] ATTEMPT";
export const ACTION_TRANSLATE_SUCCESS = "[translate] SUCCESS";
export const ACTION_TRANSLATE_ERROR = "[translate] ERROR";
export const ACTION_TRANSLATE_CLEAR = "[translate] CLEAR";

export const translateAttemptAction = (payload: any) => ({
  type: ACTION_TRANSLATE_ATTEMPTING,
  payload,
});

export const translateSuccessAction = (user: UserType) => ({
  type: ACTION_TRANSLATE_SUCCESS,
  payload: user,
});

export const translateErrorAction = (error: UserType) => ({
  type: ACTION_TRANSLATE_ERROR,
  payload: error,
});

export const translateClearAction = () => ({
  type: ACTION_TRANSLATE_CLEAR,
});
