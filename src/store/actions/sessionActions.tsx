import { UserType } from "../../utils/types";

export const ACTION_SESSION_SET = "[SESSION] SET";
export const ACTION_SESSION_INIT = "[SESSION] INIT";
export const ACTION_SESSION_CLEAR_ATTEMPT = "[SESSION CLEAR] ATTEMPT";
export const ACTION_SESSION_CLEAR_SUCCESS = "[SESSION CLEAR] SUCCESS";

export const sessionSetAction = (user: UserType) => ({
  type: ACTION_SESSION_SET,
  payload: user,
});

export const sessionInitAction = () => ({
  type: ACTION_SESSION_INIT,
});

export const sessionClearAttemptAction = () => ({
  type: ACTION_SESSION_CLEAR_ATTEMPT,
});

export const sessionClearSuccessAction = () => ({
  type: ACTION_SESSION_CLEAR_SUCCESS,
});
