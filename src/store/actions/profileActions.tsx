import { UserType } from "../../utils/types";

export const ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT =
  "[profile clear] ATTEMPT";

export const ACTION_PROFILE_CLEAR_TRANSLATION_SUCCESS =
  "[profile clear] SUCCESS";

export const ACTION_PROFILE_QUERY_USER_ATTEMPT = "[profile query] ATTEMPT";
export const ACTION_PROFILE_QUERY_USER_ERROR = "[profile query] ERROR";
export const ACTION_PROFILE_QUERY_USER_SUCCESS = "[profile query] SUCCESS";

export const actionClearTranslationsAttempt = (payload: UserType) => ({
  type: ACTION_PROFILE_CLEAR_TRANSLATIONS_ATTEMPT,
  payload,
});

export const actionClearTranslationsSuccess = (payload: UserType) => ({
  type: ACTION_PROFILE_CLEAR_TRANSLATION_SUCCESS,
  payload,
});

export const actionQueryUserAttempt = (payload: UserType) => ({
  type: ACTION_PROFILE_QUERY_USER_ATTEMPT,
  payload,
});

export const actionQueryUserError = (payload: UserType) => ({
  type: ACTION_PROFILE_QUERY_USER_ERROR,
  payload,
});

export const actionQueryUserSuccess = (payload: UserType) => ({
  type: ACTION_PROFILE_QUERY_USER_SUCCESS,
  payload,
});
