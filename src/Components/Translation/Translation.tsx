import React, { useEffect, useState } from "react";
import "./Translation.css";
import InputFieldComponent from "../Common/InputFieldComponent";
import TranslationsContainer from "../Common/TranslationsContainer";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import {
  translateAttemptAction,
  translateClearAction,
} from "../../store/actions/translateActions";
import AuthenticatedCheck from "../HOC/AuthenticatedCheck";
import Container from "../HOC/Container";

const Translation = () => {
  const dispatch = useDispatch();
  const [translation, setTranslation] = useState("");

  const { user, activeTranslation } = useSelector(
    (state: RootStateOrAny) => state.sessionReducer
  );

  useEffect(() => {
    dispatch(translateClearAction());
  }, []);

  const onClickHandler = () => {
    if (translation.trim() === "") {
      return;
    }

    dispatch(
      translateAttemptAction({
        user: user,
        translation: translation,
      })
    );
  };

  const onTranslationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTranslation(event.target.value?.slice(-60));
  };

  return (
    <AuthenticatedCheck ShouldBeAuthorized={true}>
      <>
        <div id="welcome-container">
          <div id="Translation-input-container">
            <InputFieldComponent
              buttonIcon="arrow_forward"
              placeholder="Enter Sentence"
              borderColor="white"
              onChangeHandler={onTranslationChange}
              onClickHandler={onClickHandler}
            />
          </div>
        </div>
        <Container ContainerHeight={275} bottomColorPercent={88}>
          <TranslationsContainer translation={activeTranslation} />
        </Container>
      </>
    </AuthenticatedCheck>
  );
};

export default Translation;
