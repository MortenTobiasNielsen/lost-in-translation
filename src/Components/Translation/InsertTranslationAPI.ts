import { triviaAPI, apiKey } from "../../utils/connectionParameters";
import { UserType, errorUser } from "../../utils/types";

/**
 * Update translations for user
 * @param userId
 * @param translation
 */
export async function UpdateTranslations(
  userId: number,
  translations: Array<string>
): Promise<UserType> {
  try {
    const response = await fetch(`${triviaAPI}/${userId}`, {
      method: "PATCH",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        translations: translations,
      }),
    });

    if (response.ok) {
      return response.json();
    }

    return errorUser;
  } catch (e: any) {
    console.log(e.message);
    return errorUser;
  }
}
