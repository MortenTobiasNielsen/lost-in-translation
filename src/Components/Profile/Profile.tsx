import React, { useEffect } from "react";
import "./Profile.css";
import Button from "../Common/Button";
import TranslationsContainer from "../Common/TranslationsContainer";
import AuthenticatedCheck from "../HOC/AuthenticatedCheck";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import {
  actionClearTranslationsAttempt,
  actionQueryUserAttempt,
} from "../../store/actions/profileActions";
import Container from "../HOC/Container";

const Profile = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state: RootStateOrAny) => state.sessionReducer);

  useEffect(() => {
    dispatch(actionQueryUserAttempt(user));
  }, []);

  const ClearButtonHandler = () => {
    dispatch(actionClearTranslationsAttempt(user));
  };

  const translations = user.translations
    .map((translation: string, index: number) => (
      <div key={"tranlation-div" + index}>
        <span id="translation-container-text">{translation}</span>
        <Container ContainerHeight={275} bottomColorPercent={88}>
          <TranslationsContainer translation={translation} />
        </Container>
      </div>
    ))
    .slice(0, 10);

  return (
    <AuthenticatedCheck ShouldBeAuthorized={true}>
      <main className="Profile">
        <div id="delete-translations-container">
          <h3 style={{ paddingRight: "1rem" }}>Delete translations</h3>
          <Button
            icon={"delete"}
            color="red"
            onClickHandler={ClearButtonHandler}
          />
        </div>
        <div>{translations}</div>
      </main>
    </AuthenticatedCheck>
  );
};

export default Profile;
