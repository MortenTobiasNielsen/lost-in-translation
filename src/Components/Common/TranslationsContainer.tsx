import React from "react";

interface Props {
  translation: string;
}

const TranslationsContainer = ({ translation }: Props) => {
  const translationImages = translation
    .toLowerCase()
    .split("")
    .map((char: string, index: number): JSX.Element | string | void => {
      if (/[a-zA-Z]/.test(char)) {
        const imagePath = `/signImages/${char}.png`;
        return (
          <img
            src={imagePath}
            alt="Character as sign language"
            width={50}
            height={50}
            key={translation + "image" + index}
          />
        );
      }
      if (char === " ") {
        return (
          <span
            key={translation + "span" + index}
            style={{
              fontFamily: "serif",
              fontSize: "2rem",
              fontWeight: "bold",
              paddingLeft: "0.5rem",
              paddingRight: "0.5rem",
            }}
          >
            {"|"}
          </span>
        );
      }
    });

  return <div>{translationImages}</div>;
};

export default TranslationsContainer;
