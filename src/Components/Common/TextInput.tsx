import React, { KeyboardEvent } from "react";
import "./TextInput.css";

interface Props {
  placeholder: string;
  onChangeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onClickHandler: () => void;
}

const TextInput = ({ placeholder, onChangeHandler, onClickHandler }: Props) => {
  const pressEnter = (e: KeyboardEvent) => {
    if (e.key === "Enter") {
      onClickHandler();
    }
  };

  return (
    <input
      autoFocus
      type="text"
      placeholder={placeholder}
      onChange={onChangeHandler}
      onKeyDown={pressEnter}
    />
  );
};
export default TextInput;
