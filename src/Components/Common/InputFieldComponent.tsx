import React from "react";
import "./InputFieldComponent.css";
import TextInput from "./TextInput";
import Button from "./Button";

interface Props {
  buttonIcon: string;
  placeholder: string;
  borderColor: string;
  onChangeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onClickHandler: () => void;
}

const InputFieldComponent = ({
  buttonIcon,
  placeholder,
  borderColor,
  onChangeHandler,
  onClickHandler,
}: Props) => {
  return (
    <div id="input-container" style={{ borderColor: borderColor }}>
      <div id="input-icon-container">
        <span id="keyboard-icon" className="material-icons">
          keyboard
        </span>
        <div id="keyboard-separator"></div>
        <TextInput
          placeholder={placeholder}
          onChangeHandler={onChangeHandler}
          onClickHandler={onClickHandler}
        />
      </div>

      <Button
        icon={buttonIcon}
        color="#845EC2"
        onClickHandler={onClickHandler}
      />
    </div>
  );
};

export default InputFieldComponent;
