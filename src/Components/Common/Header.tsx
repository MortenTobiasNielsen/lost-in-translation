import React from "react";
import "./Header.css";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import Button from "./Button";
import { Route, Switch, useHistory } from "react-router-dom";
import { actionQueryUserAttempt } from "../../store/actions/profileActions";
import { sessionClearAttemptAction } from "../../store/actions/sessionActions";

const Header = (props: { title: string }) => {
  const { loggedIn, user, userProfileQueried } = useSelector(
    (state: RootStateOrAny) => state.sessionReducer
  );

  const profilePath = "/profile";
  const history = useHistory();
  const dispatch = useDispatch();
  const redirectToProfile = () => {
    history.push(profilePath);
  };

  const onClickLogoutHandler = () => {
    if (confirm("Are you sure you want to logout?")) {
      dispatch(sessionClearAttemptAction());
    }
  };

  return (
    <header id="top-header">
      <div id="header-content-container">
        <div id="header-text-icon-container">
          {loggedIn && <img src="/Logo.png" width="50" height="50" />}
          <span
            onClick={() => history.push("/")}
            style={{ cursor: "pointer" }}
            id="header-text"
          >
            {props.title}
          </span>
        </div>

        {loggedIn && (
          <Switch>
            <Route path={profilePath}>
              <div id="profile-container">
                <span id="Username-span">{`Logout ${user.username}`}</span>
                <Button
                  icon={"logout"}
                  color="#E7B355"
                  onClickHandler={onClickLogoutHandler}
                />
              </div>
            </Route>
            <Route>
              <div id="profile-container">
                <span id="Username-span">{user.username}</span>
                <Button
                  icon={"person"}
                  color="#E7B355"
                  onClickHandler={redirectToProfile}
                />
              </div>
            </Route>
          </Switch>
        )}
      </div>
    </header>
  );
};

export default Header;
