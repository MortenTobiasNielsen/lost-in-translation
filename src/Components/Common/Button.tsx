import React from "react";
import "./Button.css";
interface Props {
  icon: string;
  color: string;
  onClickHandler: () => void;
}

const Button = ({ onClickHandler, icon, color }: Props) => {
  return (
    <button
      className="RoundedButton"
      style={{ backgroundColor: color }}
      onClick={onClickHandler}
    >
      <span id="button-icon" className="material-icons">
        {icon}
      </span>
    </button>
  );
};
export default Button;
