import React, { useState } from "react";
import InputFieldComponent from "../Common/InputFieldComponent";
import LoginWelcomeText from "./LoginWelcomeText";
import { useDispatch } from "react-redux";
import { loginAttemptAction } from "../../store/actions/loginActions";
import AuthenticatedCheck from "../HOC/AuthenticatedCheck";
import Container from "../HOC/Container";

const Login = () => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");

  const onUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const onClickHandler = () => {
    dispatch(loginAttemptAction(username));
  };

  return (
    <AuthenticatedCheck ShouldBeAuthorized={false}>
      <main className="Login">
        <LoginWelcomeText />
        <Container ContainerHeight={110} bottomColorPercent={80}>
          <InputFieldComponent
            buttonIcon="arrow_forward"
            placeholder="Enter username"
            borderColor="#969696"
            onChangeHandler={onUsernameChange}
            onClickHandler={onClickHandler}
          />
        </Container>
      </main>
    </AuthenticatedCheck>
  );
};

export default Login;
