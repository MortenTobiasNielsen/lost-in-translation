import { triviaAPI, apiKey } from "../../utils/connectionParameters";
import { UserType, errorUser } from "../../utils/types";

/**
 * Fetch specific user.
 * @param username
 */
export const fetchUser = async (username: string): Promise<UserType> => {
  try {
    const response = await fetch(`${triviaAPI}?username=${username}`);
    const userArray = await response.json();

    return userArray[0];
  } catch (e: any) {
    console.log(e.message);

    return errorUser;
  }
};

/**
 * Create user
 * @param username
 */
export async function createUser(username: string): Promise<UserType> {
  try {
    const response = await fetch(triviaAPI, {
      method: "POST",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        translations: [],
      }),
    });

    if (response.ok) {
      return await response.json();
    }

    return errorUser;
  } catch (e: any) {
    return errorUser;
  }
}
