import React from "react";
import "./LoginWelcomeText.css";

const LoginWelcomeText = () => {
  return (
    <div id="welcome-container">
      <div id="welcome-content-container">
        <img src="/Logo-Hello.png" width="175" height="175" />
        <div id="welcome-text-container">
          <h1>Lost in Translation</h1>
          <h4>Get started</h4>
        </div>
      </div>
    </div>
  );
};
export default LoginWelcomeText;
