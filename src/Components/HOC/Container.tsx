import React from "react";
import "./Container.css";

type Props = {
  ContainerHeight: number;
  bottomColorPercent: number;
  children: JSX.Element;
};

const Container = ({
  children,
  ContainerHeight,
  bottomColorPercent,
}: Props) => {
  return (
    <div
      id="Container"
      style={{
        height: ContainerHeight,
        backgroundImage: `linear-Gradient(
      to bottom,
      rgba(0, 0, 0, 0) ${bottomColorPercent}%,
      #845ec2 0%
    )`,
      }}
    >
      {children}
    </div>
  );
};

export default Container;
