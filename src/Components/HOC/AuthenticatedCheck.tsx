import React from "react";
import { RootStateOrAny, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

type Props = {
    ShouldBeAuthorized: boolean,
    children: JSX.Element,
  };

const AuthenticatedCheck = ({ShouldBeAuthorized, children}: Props) => {
  const {loggedIn} = useSelector((state: RootStateOrAny) => state.sessionReducer)

  if (ShouldBeAuthorized) {
    if(loggedIn) {
      return children
    }

    return <Redirect to="/login" />
  } else {
    
    if (!loggedIn) {
      return children
    }
    
    return <Redirect to="/" />
  }
}

export default AuthenticatedCheck;