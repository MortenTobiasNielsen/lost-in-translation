import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Translation from "./Components/Translation/Translation";
import Login from "./Components/Login/Login";
import Profile from "./Components/Profile/Profile";
import Header from "./Components/Common/Header";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header title="Lost in Translation" />
        <Switch>
          <Route path="/" exact component={Translation} />
          <Route path="/login" exact component={Login} />
          <Route path="/profile" exact component={Profile} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
