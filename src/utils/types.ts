export interface UserType {
  id: number;
  username: string;
  translations: Array<string>;
}

export interface ActionType {
  type: string;
  payload?: any;
}

export const errorUser: UserType = {
  id: -1,
  username: "",
  translations: [],
};
