export const triviaAPI =
  "https://experis-fall2021-assignments.herokuapp.com/translations";
export const apiKey = "experis-assignment-api-key";
export const sessionStorageName = "UserProfile";
